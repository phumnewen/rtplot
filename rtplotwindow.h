#ifndef RTPLOTWINDOW_H
#define RTPLOTWINDOW_H

#include <iostream>

#include <QMainWindow>
#include <QSpinBox>
#include <QChartView>
#include <QScatterSeries>
#include <QLineSeries>

#include "chart.h"

using namespace QtCharts;

QT_BEGIN_NAMESPACE
namespace Ui { class RTPlotWindow; }
QT_END_NAMESPACE

class RTPlotWindow : public QMainWindow
{
    Q_OBJECT

public:

    RTPlotWindow(QWidget *parent = nullptr);
    ~RTPlotWindow();

    // Перечисление статусов генератора, которые окно может обработать
    enum class GeneratorStatus
    {
        notRunning,
        paused,
        running,
    };

    // Выставленный пользователем интервал генерации
    unsigned delayTime () const {return delay_time_ms; }

signals:

    // Сигналы от кнопок
    void runPause_pressed ();
    void stop_pressed ();

public slots:

    // Слот для изменения состояния интерфейса в зависимости от состояния расчёта
    void setRunningState (GeneratorStatus status);

    // Добавление точки на график
    void addPoint (int value);

protected slots:

    // Очистка графика
    void clearChart ();

private:

    void initializeTools ();
    void initializeChart ();

    Ui::RTPlotWindow *ui;

    QSpinBox * spinBox_delay_ms = nullptr;
    QLineSeries * series = nullptr;
    RTChart * chart = nullptr;
    QChartView * chartView = nullptr;

    unsigned delay_time_ms = 10;
    unsigned time_ms = 0;

    int xmin = 0, xmax = 1;
    int ymin = -1, ymax = 1;

};
#endif // RTPLOTWINDOW_H
