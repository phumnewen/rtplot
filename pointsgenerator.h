#ifndef POINTSGENERATOR_H
#define POINTSGENERATOR_H

#include <iostream>
#include <random>
#include <QObject>
#include <QThread>


class PointsGenerator : public QObject
{
    Q_OBJECT

public:

    explicit PointsGenerator(QObject *parent = nullptr) : QObject(parent) {}


    // Задержка в мс генерации числа
    unsigned delay() const;
    // Установить задержкгенерации числа (мс)
    void setDelay(const unsigned &delay);

    // Остановить генерацию
    void setIsStopped(bool *stopped);

    // Поставить генерацию на паузу, если paused == true, или возобновить иначе
    void setIsPaused(bool *paused);

signals:

    //  Сигнал о завершении генерации
    void finished ();

    // Сигнал, что значение сгенирировано
    void pointGenerated (int p);

public slots:

    // Слот вызывающий метод генерации
    void process ();

private:

    bool *isPaused = nullptr;
    bool *isStopped = nullptr;
    unsigned delay_ms = 0;  //msec

    // Метод генерации числа
    void generate ();

};

#endif // POINTSGENERATOR_H
