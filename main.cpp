#include "rtpcore.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    RTPCore c;
    c.showMainWindow();

    return a.exec();
}
