#include "chart.h"

RTChart::RTChart(QGraphicsItem *parent, Qt::WindowFlags wFlags)
    : QChart(parent, wFlags)
{
    legend()->hide();
    setTitle("График реального времени");

    // Настройка осей графика
    axisX = new QValueAxis();
    axisX->setTitleText("Время, мc");
    axisX->setLabelFormat("%g");
    axisX->setTickCount(5);
    axisX->setRange(0, 1);
    addAxis(axisX, Qt::AlignBottom);

    axisY = new QValueAxis();
    axisY->setTitleText("Случайный параметр");
    axisY->setLabelFormat("%g");
    axisY->setTickCount(5);
    axisY->setRange(-1, 1);
    addAxis(axisY, Qt::AlignLeft);
}
