#ifndef CHARTWIDGET_H
#define CHARTWIDGET_H

#include <QChart>
#include <QValueAxis>


using namespace QtCharts;

class RTChart : public QChart
{
    Q_OBJECT
public:

    explicit RTChart(QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = Qt::WindowType::Widget);

    QValueAxis * xAxis () const { return axisX; }
    QValueAxis * yAxis () const { return axisY; }

private:

    QValueAxis *axisX = nullptr;
    QValueAxis *axisY = nullptr;

};

#endif // CHARTWIDGET_H
