#include "pointsgenerator.h"

void PointsGenerator::process()
{
    generate();

    emit finished();
}

void PointsGenerator::generate()
{
    // Проверка, что указатели управляющих булок инициализированы
    if (!isStopped || !isPaused)
        return;

    // Создаём рандомное распределение
    std::random_device dvc;
    std::mt19937 gen (dvc());
    std::uniform_int_distribution<> dist(-10000, 10000);

    // В цикле, пока не остановят,
    // если генератор не поставлен на паузу,
    // генерируем число
    while (!*isStopped)
    {
        std::cout << "";

        if (!*isPaused)
        {
            // Держим паузу
            QThread::msleep(delay_ms);

            // Генерируем число и эмитим его сигналом
            int val = dist(gen);
            emit pointGenerated(val);
        }
    }
}

unsigned PointsGenerator::delay() const
{
    return delay_ms;
}

void PointsGenerator::setDelay(const unsigned &delay_ms)
{
    this->delay_ms = delay_ms;
}

void PointsGenerator::setIsPaused(bool *value)
{
    isPaused = value;
}

void PointsGenerator::setIsStopped(bool *value)
{
    isStopped = value;
}
