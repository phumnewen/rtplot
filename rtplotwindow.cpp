#include "rtplotwindow.h"
#include "ui_rtplotwindow.h"

RTPlotWindow::RTPlotWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::RTPlotWindow)
{
    ui->setupUi(this);

    initializeTools();

    initializeChart();

}

RTPlotWindow::~RTPlotWindow()
{
    delete ui;
}

void RTPlotWindow::initializeTools()
{
    // Кнопка стоп
    ui->action_stop->setIcon(QIcon(":/toolbuttons/icons/stop.png"));
    ui->action_stop->setToolTip(tr("Остановить"));

    // Кнопка запуск/пауза
    ui->action_runPause->setIcon(QIcon(":/toolbuttons/icons/play.png"));
    ui->action_runPause->setToolTip(tr("Запустить"));

    // Спинбокс периода генерации
    spinBox_delay_ms = new QSpinBox(this);
    spinBox_delay_ms->setToolTip(tr("Период генерации чисел"));
    spinBox_delay_ms->setRange(1, 2000);
    spinBox_delay_ms->setValue(static_cast<int>(delay_time_ms));

    ui->toolBar->addWidget(spinBox_delay_ms);

    // Коннекты событий от кнопок и спинбокса
    connect (ui->action_quit, &QAction::triggered, this, &QMainWindow::close);
    connect (ui->action_runPause, &QAction::triggered, this, &RTPlotWindow::runPause_pressed);
    connect (ui->action_stop, &QAction::triggered, this, &RTPlotWindow::stop_pressed);
    connect (spinBox_delay_ms, static_cast<void (QSpinBox::*)(int)> ( &QSpinBox::valueChanged ),
             this, [=](int val) { delay_time_ms = static_cast<unsigned>(val); } );
}

void RTPlotWindow::initializeChart()
{
    // График
    chart = new RTChart ();

    // Серия данных
    series = new QLineSeries (this);
    series->setPen(QPen(QBrush(Qt::blue), 3));
//    series->setBrush(Qt::blue);

    // Добавляем данные в серию
    chart->addSeries(series);

    series->attachAxis(chart->xAxis());
    series->attachAxis(chart->yAxis());

    chartView = new QChartView (chart, this);
    chart->setParent(chartView);
    ui->verticalLayout->addWidget(chartView);
}

void RTPlotWindow::setRunningState(RTPlotWindow::GeneratorStatus status)
{
    // Изменение периода генерации доступно только при незапущенном генераторе
    spinBox_delay_ms->setEnabled(false);

    // Изменение иконки и текста подсказки кнопки "запуск/пауза"
    // при изменении статуса генератора
    switch (status)
    {
    case GeneratorStatus::running:
        ui->action_runPause->setIcon(QIcon(":/toolbuttons/icons/pause.png"));
        ui->action_runPause->setToolTip(tr("Приостановить"));
        break;
    case GeneratorStatus::notRunning:
        spinBox_delay_ms->setEnabled(true);
        ui->action_runPause->setIcon(QIcon(":/toolbuttons/icons/play.png"));
        ui->action_runPause->setToolTip(tr("Запусить"));
        clearChart();
        break;
    case GeneratorStatus::paused:
        ui->action_runPause->setIcon(QIcon(":/toolbuttons/icons/play.png"));
        ui->action_runPause->setToolTip(tr("Возобновить"));
        break;
    }
}

void RTPlotWindow::addPoint(int value)
{
    // Добавление точки с новым сгенерированным значением на график

    // x-координата - это время с момента начала генерации
    time_ms += delay_time_ms;
    series->append(time_ms, value);

    // Обновляем пределы осей
    xmax = static_cast<int>(time_ms);

    if (value > ymax)
        ymax  = value;
    if (value < ymin)
        ymin  = value;

    chart->xAxis()->setRange(xmin, xmax);
    chart->yAxis()->setRange(ymin, ymax);
}

void RTPlotWindow::clearChart()
{
    // Чистим данные
    series->clear();

    // Сбрасываем время
    time_ms = 0;

    // Сбрасываем пределы осей
    ymin = -1;
    ymax = 1;
    xmin = 0;
    xmax = 1;

    chart->xAxis()->setRange(xmin, xmax);
    chart->yAxis()->setRange(ymin, ymax);
}
