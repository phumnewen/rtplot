#include "rtpcore.h"

RTPCore::RTPCore(QObject *parent) : QObject(parent)
{
    // Инициализируем окно, устанавливаем ему аттрибут удаления при закрытии
    mw = new RTPlotWindow ();
    mw->setAttribute(Qt::WA_DeleteOnClose);

    // Пробрасываем коннекты между кором и окном

    connect(this, &RTPCore::generatorStateChanged, mw, &RTPlotWindow::setRunningState);
    connect(this, &RTPCore::pointGenerated, mw, &RTPlotWindow::addPoint);

    connect(mw, &RTPlotWindow::runPause_pressed, this, &RTPCore::runOrPauseGenerator);
    connect(mw, &RTPlotWindow::stop_pressed, this, &RTPCore::stopGenerator);
    connect(mw, &RTPlotWindow::destroyed, this, &RTPCore::stopGenerator);
}

void RTPCore::showMainWindow()
{
    mw->show();
}

void RTPCore::runOrPauseGenerator()
{
    switch (status)
    {
    case RTPlotWindow::GeneratorStatus::running:    // Pause generator
        pauseGenerator_ = true;
        status = RTPlotWindow::GeneratorStatus::paused;
        std::cout << "Generator paused" << std::endl;
        emit generatorStateChanged (status);
        break;
    case RTPlotWindow::GeneratorStatus::notRunning: // Run generator
        runGenerator();
        status = RTPlotWindow::GeneratorStatus::running;
        std::cout << "Generator started" << std::endl;
        emit generatorStateChanged (status);
        break;
    case RTPlotWindow::GeneratorStatus::paused:     // Resume generator
        pauseGenerator_ = false;
        status = RTPlotWindow::GeneratorStatus::running;
        std::cout << "Generator resumed" << std::endl;
        emit generatorStateChanged (status);
        break;
    }
}

void RTPCore::stopGenerator()
{
    stopGenerator_ = true;
    if (generatorThread)
        generatorThread->wait();
    status = RTPlotWindow::GeneratorStatus::notRunning;
}

void RTPCore::runGenerator()
{
    // Запуск генератора чисел в отдельном потоке
    generatorThread = new QThread (this);
    PointsGenerator *generator = new PointsGenerator ();
    generator->moveToThread(generatorThread);

    pauseGenerator_ = false;
    stopGenerator_ = false;

    generator->setIsPaused(&pauseGenerator_);
    generator->setIsStopped(&stopGenerator_);
    generator->setDelay(mw->delayTime());

    connect (generatorThread, &QThread::started, generator, &PointsGenerator::process);
    connect (generator, &PointsGenerator::finished, [=]()
    {
        generatorThread->quit();
        std::cout << "Generator stopped" << std::endl;
    });
    connect (generatorThread, &QThread::finished, [=]()
    {
        std::cout << "Generator thread finished" << std::endl;
        generatorThread->deleteLater();
        generatorThread = nullptr;
        std::cout << "Generator thread destroyed" << std::endl;
    });

    connect (generatorThread, &QThread::finished, generator, &PointsGenerator::deleteLater);

    connect (generator, &PointsGenerator::pointGenerated, this, &RTPCore::pointGenerated);
    connect (generatorThread, &QThread::finished, this, [=]()
    {
        emit generatorStateChanged (RTPlotWindow::GeneratorStatus::notRunning);
    } );

    generatorThread->start();
}
