#ifndef RTPCORE_H
#define RTPCORE_H

#include <QThread>
#include <QApplication>

#include "pointsgenerator.h"
#include "rtplotwindow.h"


// Главный класс приложения,
// обеспечивает взаимодействие всех компонентов

class RTPCore : public QObject
{
    Q_OBJECT

public:

    explicit RTPCore(QObject *parent = nullptr);

public slots:

    // Открытие окна приложения
    void showMainWindow ();

    // Обработка сигналов от окна
    void runOrPauseGenerator ();
    void stopGenerator ();

signals:

    // Сигналы окну об изменениях в генераторе чисел
    void generatorStateChanged (RTPlotWindow::GeneratorStatus status);
    void pointGenerated (int p);

private:

    // Запуск генератора в отдельном потоке
    void runGenerator ();

    RTPlotWindow *mw = nullptr;

    // Поток генератора чисел
    QThread *generatorThread = nullptr;

    // Статус генератора
    RTPlotWindow::GeneratorStatus status = RTPlotWindow::GeneratorStatus::notRunning;

    // Булки для управления генератором
    bool stopGenerator_ = false;
    bool pauseGenerator_ = false;

};

#endif // RTPCORE_H
